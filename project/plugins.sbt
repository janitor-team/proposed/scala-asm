addSbtPlugin("org.scala-lang.modules" % "sbt-scala-module" % "1.0.12")

// Last version that works on Java 6
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.8.5")
